#!/bin/sh

# git-autosync
# Copyright (C) 2020  Emily Rowlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, using version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# The config file for this script

: "${CONFIG_FILE:=/etc/git-autosync}"

# The directory to store all of the bare repos in
: "${GIT_BASEDIR:=/var/lib/git}"

the_log() {
	logger -t git-autosync $@
}

log() {
	the_log -p user.info $@
	echo - $@
}

log_error() {
	the_log -p user.error $@
	echo - $@ >&2
}

log "Starting sync"

# $repo     the full git url (INCLUDING PASSWORDS)
# $name     the name of the repo (must be unique)
# $tree_dir the path to clone to
# $branch   the branch to use (empty for default)
< $CONFIG_FILE grep -vE '^(#|$)' | while read repo name tree_dir branch; do
	git_dir="$GIT_BASEDIR/$name.git"

	log "$name: Starting sync"
	if [ ! -d "$tree_dir" ]; then
		log "$name: Creating new tree directory: $tree_dir"
		# Create the workdir to put all of the cloned files in
		mkdir "$tree_dir" || { log_error "$name: Failed to create target directory $tree_dir"; continue; }
	fi

	if [ ! -d "$git_dir" ]; then
		log "$name: Creating new repo in $git_dir"
		# Clone a bare git repo
		# This means that the .git directory is in $git_dir, and no files are checked out
		brancharg="--branch $branch"
		if [ -z "$branch" ]; then
			brancharg=""
		fi
		git clone ${brancharg} --bare "$repo" "$git_dir" || { log_error "$name: Failed to clone repository in $git_dir"; continue; }
	else
		log "$name: fetching"
		# If the git dir already exists, just fetch updates
		git -C "$git_dir" fetch -f || { log_error "$name: Failed to fetch"; continue; }
	fi
	log "$name: Checking out files"
	# Check out files to $tree_dir using the bare git repo in $git_dir
	git --work-tree="$tree_dir" --git-dir="$git_dir" checkout -f $branch || log_error "$name: Failed to checkout $git_dir into $tree_dir"
	log "$name: Sync complete"
# Read the config file into the loop
done
